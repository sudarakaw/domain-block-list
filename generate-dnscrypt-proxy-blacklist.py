#!/usr/bin/env python
# generate-dnscrypt-proxy-blacklist.py: convert raw domain list to DNSCrypt-proxy compatible blacklist
#
# Copyright 2019 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#
# This program comes with ABSOLUTELY NO WARRANTY
# This is free software, and you are welcome to redistribute it and/or modify
# it under the terms of the BSD 2-clause License. See the LICENSE file for more
# details.
#

from datetime import datetime
from urllib.request import urlopen

import re
import sys

WILDCARD_PATTERNS = { 'ad.*': r'^ad\.'
                    , 'ad[0-9]*': r'^ad[\d]+'
                    , 'ads.*': r'^ads\.'
                    , 'ads[0-9]*': r'^ads[\d]+'
                    , 'count*.*': r'^count.*\.'
                    , 'openad*.*': r'^openad.*\.'
                    , 'openx.*': r'^openx\.'
                    }

domains = {}
wildcards = set()


def get_local_list():
  with open('blacklist-raw.txt', 'r') as raw_file:
    return raw_file.read().splitlines()


def get_steven_black_host_list():
  response = urlopen('https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews/hosts')
  lines = [ l.split(None, 2)[1] for l in response.read().decode('utf-8').splitlines() if is_host_entry(l)]

  return lines


def is_host_entry(text):
  # Exlucde lines NOT containing block entry
  if '0.0.0.0' != text[:7]:
    return False

  if '0.0.0.0' == text[-7:]:
    return False

  return True


if '__main__' == __name__:
  domain_list = set(get_local_list() + get_steven_black_host_list())

  for domain in domain_list:
    # skip comments and blank lines
    if '\n' == domain or '#' == domain[:1]:
      continue

    wild_match = [ w
                  for (w, r)
                  in WILDCARD_PATTERNS.items()
                  if re.match(r, domain)
                 ]

    if 0 < len(wild_match):
      wildcards.update(wild_match)

      continue

    tld = domain.split('.')[-1]

    if tld not in domains:
      domains[tld] = []

    domains[tld].append(domain)

  # Print header
  print('''
# blacklist.txt: DNSCrypt-proxy blacklist
# Generated: %s
'''.lstrip()
% (datetime.now()))

  # Print applicable block patterns & domain names
  print('''
# Pattern based blocks
%s
'''.strip()
% ('\n'.join(wildcards)))

  for tld in sorted(domains):
    print()
    print('# TLD:', tld)
    print('\n'.join(sorted(domains[tld])))
